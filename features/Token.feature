# @author Cesare s232435
Feature: Token Management

  Scenario: Successful Token Generation
    Given I am a DTUPay customer
    When I request 4 tokens
    Then I should receive 4 tokens

  Scenario: Failed Token Generation
    Given I am a DTUPay customer
    When I request 2 tokens
    Then I should receive 2 tokens
    And I request 4 tokens
    Then I should receive an error message "Customer already has available tokens"

  Scenario: Failed Token Validated
    Given I am a DTUPay customer
    When I request 2 tokens
    Then I should receive 2 tokens
    And I pay with 1 token
    When I try pay with the same token
    Then I should receive an error message "Token not validated"

  Scenario: Failed Token Validation when requesting more than 5 tokens
    Given I am a DTUPay customer
    When I request 6 tokens
    Then I should receive an error message "Amount of tokens requested must be between 1 and 5"

  Scenario: Failed Token Validation when requesting 0 tokens
    Given I am a DTUPay customer
    When I request 0 tokens
    Then I should receive an error message "Amount of tokens requested must be between 1 and 5"
