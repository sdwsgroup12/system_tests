# @author Can s232851
Feature: Payment

  Scenario: Successful Payment
    Given a customer with CPR "123456-10", first name "Bob", last name "Red", and bank with balance 1000
    And that the customer is registered with DTU Pay
    Given a merchant with CPR "123456-70", first name "Bob", last name "Red", and bank with balance 2000
    And that the merchant is registered with DTU Pay
    Given that the customer gets 5 tokens from the DTU Pay system
    And that the customer chooses one of the tokens
    When the merchant initiates a payment for 100 kr by the customer
    Then the payment is successful
    Then the balance of the customer at the bank is 900 kr
    Then the balance of the merchant at the bank is 2100 kr

  Scenario: Error when insufficient balance
    Given a customer with a bank account with balance 10
    And that the customer is registered with DTU Pay
    Given a merchant with a bank account with balance 2000
    And that the merchant is registered with DTU Pay
    Given that the customer gets 5 tokens from the DTU Pay system
    And that the customer chooses one of the tokens
    When the merchant initiates a payment for 100 kr by the customer
    Then the exception "Debtor balance will be negative" is received

  Scenario: Successful concurrent payments
    Given customer c1 with a bank account with balance 1000
    And customer c2 with a bank account with balance 2000
    And customer c3 with a bank account with balance 3000
    And the customer c1 is registered with DTU Pay
    And the customer c2 is registered with DTU Pay
    And the customer c3 is registered with DTU Pay
    Given merchant m1 with a bank account with balance 2000
    And merchant m2 with a bank account with balance 4000
    And merchant m3 with a bank account with balance 6000
    And that the merchant m1 is registered with DTU Pay
    And that the merchant m2 is registered with DTU Pay
    And that the merchant m3 is registered with DTU Pay
    Given that the customer c1 gets 1 tokens from the DTU Pay system
    Given that the customer c2 gets 2 tokens from the DTU Pay system
    Given that the customer c3 gets 3 tokens from the DTU Pay system
    And that customer c1 chooses one of the tokens
    And that customer c2 chooses one of the tokens
    And that customer c3 chooses one of the tokens
    When the merchant m1 initiates a payment for 100 kr by the customer
    And the merchant m2 initiates a payment for 200 kr by the customer
    And the merchant m3 initiates a payment for 300 kr by the customer
    When the payments start concurrently
    Then the payment is successful for customer c1 and merchant m1
    And the balance of the customer c1 at the bank is 900 kr
    And the balance of the merchant m1 at the bank is 2100 kr
    Then the payment is successful for customer c2 and merchant m2
    And the balance of the customer c2 at the bank is 1800 kr
    And the balance of the merchant m2 at the bank is 4200 kr
    Then the payment is successful for customer c3 and merchant m3
    And the balance of the customer c3 at the bank is 2700 kr
    And the balance of the merchant m3 at the bank is 6300 kr

  Scenario: Failure when using a fake token
    Given a customer with CPR "123456-10", first name "Bob", last name "Red", and bank with balance 1000
    And that the customer is registered with DTU Pay
    Given a merchant with CPR "123456-70", first name "Bob", last name "Red", and bank with balance 2000
    And that the merchant is registered with DTU Pay
    Given that the customer gets 5 tokens from the DTU Pay system
    And that the customer gives a fake token
    When the merchant initiates a payment for 100 kr by the customer
    Then the exception "Token not validated" is received
