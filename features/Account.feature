# Cesare s232435
Feature: Account management

  Scenario: Successful customer account deregistration
    Given a customer with CPR "123456-10", first name "Bob", last name "Red"
    And that the customer is registered through DTU Pay
    When the customer deregisters from DTU Pay successfully

  Scenario: Successful merchant account deregistration
    Given a merchant with CPR "222", first name "Alice", last name "Green"
    And the merchant is registered through DTU Pay
    When the merchant deregisters from DTU Pay successfully
