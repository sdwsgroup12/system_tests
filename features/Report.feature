# @author Matteo s222952
Feature: Report Generation

  Scenario: Successful Customer Report Generation
    Given a payment exists between a customer and a merchant
    When the customer requests a report for the payment
    Then the customer report is generated successfully

  Scenario: Successful Merchant Report Generation
    Given a payment exists between a customer and a merchant
    When the merchant requests a report for the payment
    Then the merchant report is generated successfully

  Scenario: Successful Administrator Report Generation
    Given a payment exists between a customer and a merchant
    When the manager requests a report for the paymentRequest
    Then the manager report is generated successfully

  Scenario: Invalid Merchant Report Generation
    Given a merchant is not registered to DTUpay
    When the merchant requests a report for the payment
    Then an error message "Merchant not found" is obtained

  Scenario: Invalid Customer Report Generation
    Given a customer is not registered to DTUpay
    When the customer requests a report for the payment
    Then an error message "Customer not found" is obtained
