@echo off

call docker-compose down
call docker image prune -f
call docker-compose up -d rabbitMq dtupay_facade
timeout /nobreak /t 20 > nul
call docker-compose up -d dtupay_payment dtupay_accountmanagement dtupay_tokenmanagement dtupay_reportgeneration
