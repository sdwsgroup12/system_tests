#!/bin/bash
set -e
docker-compose down
docker image prune -f
docker-compose up -d rabbitMq dtupay_facade
sleep 20
docker-compose up -d dtupay_payment dtupay_accountmanagement dtupay_reportgeneration dtupay_tokenmanagement
