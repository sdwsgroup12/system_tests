package dtu.example;

import customer.CustomerAPI;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.ManagerAPI;
import merchant.MerchantAPI;
import model.Payment;
import model.Report;
import model.Token;
import org.junit.jupiter.api.Assertions;

import java.math.BigDecimal;
import java.util.ArrayList;
// @author Abi s205720
public class ReportGenerationSteps {

    User customer = new User();
    User merchant = new User();
    BankService bank = new BankServiceService().getBankServicePort();
    String customerAccountId, merchantAccountId;
    String customerId, merchantId;
    CustomerAPI customerAPI = new CustomerAPI();
    ArrayList<Token> tokens;
    Token chosenToken;
    boolean successful;
    MerchantAPI merchantAPI = new MerchantAPI();
    ManagerAPI managerAPI = new ManagerAPI();
    int amount = 2000;
    int paymentAmount = 100;
    Report customerReport;
    Report merchantReport;
    Report managerReport;
    Exception error;

    @Given("a payment exists between a customer and a merchant")
    public void aPaymentExistsBetweenACustomerAndAMerchant() throws Exception {
        try {
            bank.retireAccount(bank.getAccountByCprNumber("22222").getId());
        } catch (Exception e) {

        }
        customer.setCprNumber("22222");
        customer.setFirstName("Test");
        customer.setLastName("Test");
        customerAccountId = bank.createAccountWithBalance(customer, BigDecimal.valueOf(amount));
        customerId = customerAPI.register(customer.getFirstName() + " " + customer.getLastName(),
                customer.getCprNumber(), customerAccountId);
        try {
            bank.retireAccount(bank.getAccountByCprNumber("44444").getId());
        } catch (Exception e) {

        }
        merchant.setCprNumber("44444");
        merchant.setFirstName("Testing Jr");
        merchant.setLastName("Testingsson");
        merchantAccountId = bank.createAccountWithBalance(merchant, BigDecimal.valueOf(amount));
        merchantId = merchantAPI.register(merchant.getFirstName() + " " + merchant.getLastName(),
                merchant.getCprNumber(), merchantAccountId);
        tokens = customerAPI.getTokens(5, customerId);
        if (!tokens.isEmpty()) {
            this.chosenToken = tokens.get(0);
        } else {
            Assertions.fail();
        }
        try {
            successful = merchantAPI.pay(chosenToken, merchantId, paymentAmount);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            successful = false;
        }
        Assertions.assertTrue(successful);
        // Write code here that turns the phrase above into concrete actions
    }

    @When("the customer requests a report for the payment")
    public void theCustomerRequestsAReportForThePayment() {
        try {
            customerReport = customerAPI.getReport(customerId);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            error = e;
        }
    }

    @Then("the customer report is generated successfully")
    public void theCustomerReportIsGeneratedSuccessfully() {
        Payment reportedPayment = customerReport.getPayments().get(0);
        Assertions.assertEquals(paymentAmount, reportedPayment.getPaymentRequest().getAmount());
        Assertions.assertEquals(merchantId, reportedPayment.getPaymentRequest().getMerchantID());
        Assertions.assertEquals(customerId, reportedPayment.getCustomerID());
        Assertions.assertEquals(chosenToken, reportedPayment.getPaymentRequest().getToken());
    }

    @After
    public void after() {
        try {
            bank.retireAccount(customerAccountId);
            bank.retireAccount(merchantAccountId);
        } catch (Exception e) {
            System.out.println("Account already retired");
        }
    }

    @When("the merchant requests a report for the payment")
    public void theMerchantRequestsAReportForThePayment() throws Exception {
        try {
            merchantReport = merchantAPI.getReport(merchantId);
        } catch (Exception e) {
            error = e;
        }
    }

    @Then("the merchant report is generated successfully")
    public void theMerchantReportIsGeneratedSuccessfully() {
        Payment reportedPayment = merchantReport.getPayments().get(0);
        Assertions.assertEquals(paymentAmount, reportedPayment.getPaymentRequest().getAmount());
        Assertions.assertEquals(merchantId, reportedPayment.getPaymentRequest().getMerchantID());
        Assertions.assertNull(reportedPayment.getCustomerID());
        Assertions.assertEquals(chosenToken, reportedPayment.getPaymentRequest().getToken());
    }

    @When("the manager requests a report for the paymentRequest")
    public void theManagerRequestsAReportForThePaymentRequest() {
        managerReport = managerAPI.getReport();
    }

    @Then("the manager report is generated successfully")
    public void theManagerReportIsGeneratedSuccessfully() {
        Payment reportedPayment = managerReport.getPayments().get(managerReport.getPayments().size() - 1);
        Assertions.assertEquals(paymentAmount, reportedPayment.getPaymentRequest().getAmount());
        Assertions.assertEquals(merchantId, reportedPayment.getPaymentRequest().getMerchantID());
        Assertions.assertEquals(customerId, reportedPayment.getCustomerID());
        Assertions.assertEquals(chosenToken, reportedPayment.getPaymentRequest().getToken());
    }

    @Then("an error message {string} is obtained")
    public void anErrorMessageIsFound(String arg0) {
        Assertions.assertEquals(arg0, error.getMessage());
    }

    @Given("a merchant is not registered to DTUpay")
    public void aMerchantIsNotRegisteredToDTUpay() {
        Assertions.assertNull(merchantId);
    }

    @Given("a customer is not registered to DTUpay")
    public void aCustomerIsNotRegisteredToDTUpay() {
        Assertions.assertNull(customerId);
    }
}
