package dtu.example;

import customer.CustomerAPI;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;
import io.cucumber.java.After;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import merchant.MerchantAPI;
import model.Token;
import org.junit.jupiter.api.Assertions;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

// @author Ion s204710
public class TokenManagementSteps {
    BankService bank = new BankServiceService().getBankServicePort();
    String customerAccountId;
    User customer = new User();
    User merchant = new User();
    CustomerAPI customerAPI = new CustomerAPI();
    String customerId;
    List<Token> currentTokens = new ArrayList<>();
    MerchantAPI merchantAPI = new MerchantAPI();
    String merchantAccountId, merchantId;

    Exception error;

    Token selectedToken;

    @Given("I am a DTUPay customer")
    public void iAmADTUPayCustomer() throws BankServiceException_Exception {
        try {
            bank.retireAccount(bank.getAccountByCprNumber("2002").getId());
        } catch (Exception e) {
        }
        customer.setFirstName("Abi");
        customer.setLastName("lu");
        customer.setCprNumber("2002");
        customerAccountId = bank.createAccountWithBalance(customer, BigDecimal.valueOf(100));
        customerId = customerAPI.register(customer.getFirstName() + customer.getLastName(), customer.getCprNumber(), customerAccountId);

    }
    @Then("I should receive {int} tokens")
    public void iShouldReceiveTokens(int arg0) {
        Assertions.assertEquals(currentTokens.size(),arg0);
    }

    @When("I request {int} tokens")
    public void iRequestToken(int arg0) {
        try {
            currentTokens = customerAPI.getTokens(arg0, customerId);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            this.error = e;
        }
    }

    @And("I have {int} tokens")
    public void iHaveTokens(int arg0) {
        Assertions.assertEquals(currentTokens.size(),arg0);
    }

    @Then("I should receive an error message {string}")
    public void iShouldReceiveAnErrorMessage(String arg0) {
        Assertions.assertEquals(arg0, error.getMessage());
    }

    @Then("I pay with {int} token")
    public void iPayWithToken(Integer int1) throws Exception {
        selectedToken = currentTokens.get(0);
        try {
            bank.retireAccount(bank.getAccountByCprNumber("2000123").getId());
        } catch (Exception e) {
        }
        merchant.setFirstName("Merchant");
        merchant.setLastName("Last name");
        merchant.setCprNumber("2000123");
        merchantAccountId = bank.createAccountWithBalance(merchant, BigDecimal.valueOf(200));
        merchantId = merchantAPI.register(merchant.getFirstName() + merchant.getLastName(), merchant.getCprNumber(), merchantAccountId);
        try {
            merchantAPI.pay(selectedToken,merchantId,20);
        } catch (Exception e){
            this.error = e;
        }
    }

    @When("I try pay with the same token")
    public void I_try_pay_with_the_same_token() {
        try {
            merchantAPI.pay(selectedToken,merchantId,20);
        } catch (Exception e){
            this.error = e;
        }
    }
    
    @After
    public void after() throws BankServiceException_Exception {
        try {
            bank.retireAccount(customerAccountId);
            bank.retireAccount(merchantAccountId);

        } catch (Exception e) {
        }
    }
}
