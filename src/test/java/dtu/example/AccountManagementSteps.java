package dtu.example;

import customer.CustomerAPI;
import dtu.ws.fastmoney.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import merchant.MerchantAPI;
import org.junit.jupiter.api.Assertions;

// @author Alexander s204092
public class AccountManagementSteps {

	User customer, merchant;
	String customerId, merchantId;
	CustomerAPI customerAPI = new CustomerAPI();
	MerchantAPI merchantAPI = new MerchantAPI();
	@Given("a customer with CPR {string}, first name {string}, last name {string}")
	public void a_customer_with_CPR_first_name_last_name(String cpr, String firstName, String lastName) {
		customer = new User();
		customer.setCprNumber(cpr);
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
	}



	@When("the customer deregisters from DTU Pay successfully")
	public void theCustomerDeregistersFromDTUPaySuccessfully() {
		System.out.println(customerId);
		Assertions.assertTrue(customerAPI.deregister(customerId));

	}

	@And("that the customer is registered through DTU Pay")
	public void thatTheCustomerIsRegisteredThroughDTUPay() {
		try {
			System.out.println("1");
			customerId = customerAPI.register(customer.getFirstName() + " " + customer.getLastName(),
					customer.getCprNumber(), "1234567890");
			System.out.println("2");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@Given("a merchant with CPR {string}, first name {string}, last name {string}")
	public void aMerchantWithCPRFirstNameLastName(String cpr, String firstName, String lastName) {
		merchant = new User();
		merchant.setCprNumber(cpr);
		merchant.setFirstName(firstName);
		merchant.setLastName(lastName);


	}

	@And("the merchant is registered through DTU Pay")
	public void theMerchantIsRegisteredThroughDTUPay() {
		try {
			merchantId = merchantAPI.register(merchant.getFirstName() + " " + merchant.getLastName(),
					merchant.getCprNumber(), "123456789011");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@When("the merchant deregisters from DTU Pay successfully")
	public void theMerchantDeregistersFromDTUPaySuccessfully() {
		Assertions.assertTrue(merchantAPI.deregister(merchantId));
	}
}
