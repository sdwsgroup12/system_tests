package dtu.example;

import customer.CustomerAPI;
import customer.CustomersServiceException;
import dtu.ws.fastmoney.*;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import merchant.MerchantAPI;
import model.Token;
import org.junit.jupiter.api.Assertions;

import java.math.BigDecimal;
import java.util.ArrayList;

// @author Matteo s222952
public class PaymentSteps {

    User customer = new User();
    User merchant = new User();
    BankService bank = new BankServiceService().getBankServicePort();
    String customerAccountId, merchantAccountId;
    String customerId, merchantId;
    CustomerAPI customerAPI = new CustomerAPI();
    ArrayList<Token> tokens;
    Token chosenToken;
    boolean successful;
    MerchantAPI merchantAPI = new MerchantAPI();
    Thread thread1, thread2, thread3;

    String exception;


    @Given("a customer with CPR {string}, first name {string}, last name {string}, and bank with balance {int}")
    public void a_customer_with_CPR_first_name_last_name_and_bank_with_balance(String cpr, String firstName,
            String lastName, int amount) throws BankServiceException_Exception {
        try {
            bank.retireAccount(bank.getAccountByCprNumber(cpr).getId());
        } catch (Exception e) {
        }
        customer.setCprNumber(cpr);
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        customerAccountId = bank.createAccountWithBalance(customer, BigDecimal.valueOf(amount));
    }
    
    @Given("that the customer is registered with DTU Pay")
    public void that_the_customer_is_registered_with_dtu_pay() {
        customerId = customerAPI.register(customer.getFirstName() + " " + customer.getLastName(),
                customer.getCprNumber(), customerAccountId);
    }

    @Given("a customer with a bank account with balance {int}")
    public void a_customer_with_a_bank_account_with_balance(int amount) throws BankServiceException_Exception {
        try {
            bank.retireAccount(bank.getAccountByCprNumber("12413432").getId());
        } catch (Exception e){

        }
        customer.setCprNumber("12413432");
        customer.setFirstName("Ion Chetraru");
        customer.setLastName("Chetraru");
        customerAccountId = bank.createAccountWithBalance(customer, BigDecimal.valueOf(amount));
    }

    @Given("a merchant with a bank account with balance {int}")
    public void a_merchant_with_a_bank_account_with_balance(int amount) throws BankServiceException_Exception {
        try {
            bank.retireAccount(bank.getAccountByCprNumber("000000000").getId());
        } catch (Exception e) {

        }
        merchant.setCprNumber("000000000");
        merchant.setFirstName("Abi");
        merchant.setLastName("Pu111");
        merchantAccountId = bank.createAccountWithBalance(merchant, BigDecimal.valueOf(amount));
    }

    @Given("a merchant with CPR {string}, first name {string}, last name {string}, and bank with balance {int}")
    public void a_merchant_with_CPR_first_name_last_name_and_bank_with_balance(String cpr, String firstName, String lastName, int amount) throws BankServiceException_Exception{
        try {
            bank.retireAccount(bank.getAccountByCprNumber("12413432").getId());
        } catch (Exception e) {

        }
        merchant.setCprNumber("12413432");
        merchant.setFirstName("Ion Chetraru");
        merchant.setLastName("Chetraru");
        merchantAccountId = bank.createAccountWithBalance(merchant, BigDecimal.valueOf(amount));
    }
    
    @Given("that the merchant is registered with DTU Pay")
    public void that_the_merchant_is_registered_with_dtu_pay() {
        merchantId = merchantAPI.register(merchant.getFirstName() + " " + merchant.getLastName(),
                merchant.getCprNumber(), merchantAccountId);
    }

    @Then("the payment is successful")
    public void the_payment_is_successful() {
        Assertions.assertTrue(successful);
    }

    @Given("that the customer gets {int} tokens from the DTU Pay system")
    public void that_the_customer_gets_tokens_from_the_dtu_pay_system(int amountTokens) throws Exception {
        tokens = customerAPI.getTokens(amountTokens, customerId);
    }

    @Given("that the customer chooses one of the tokens")
    public void that_the_customer_chooses_one_of_the_tokens() {
        if (!tokens.isEmpty()) {
            chosenToken = tokens.get(0);
        } else {
            Assertions.fail();
        }
    }

    @Given("that the customer gives a fake token")
    public void thatTheCustomerGivesAFakeToken() {
        Token token = new Token();
        token.setTokenID("fake-token");
        chosenToken = token;
    }

    @When("the merchant initiates a payment for {int} kr by the customer")
    public void the_merchant_initiates_a_payment_for_kr_by_the_customer(int amount) {
        try {
            successful = merchantAPI.pay(chosenToken, merchantId, amount);
        } catch (Exception e) {
            exception = e.getMessage();
            successful = false;
        }
    }

    @Then("the customer is registered with DTU Pay with CPR {string}")
    public void the_customer_is_registered_with_DTU_Pay_with_cpr(String cpr) {
        Assertions.assertEquals(customer.getCprNumber(), cpr);
    }

    @When("the merchant registers with DTU Pay, using CPR {string}, first name {string}, last name {string}, and bank with balance {int}")
    public void the_merchant_registers_with_DTU_Pay_using_CPR_first_name_last_name_and_bank_with_balance(String cpr, String firstName, String lastName, Integer amount) throws BankServiceException_Exception {
        try {
            bank.retireAccount(bank.getAccountByCprNumber(cpr).getId());
        } catch (Exception e){
            e.printStackTrace();
        }
        merchant.setCprNumber(cpr);
        merchant.setFirstName(firstName);
        merchant.setLastName(lastName);
        merchantAccountId = bank.createAccountWithBalance(merchant, BigDecimal.valueOf(amount));
    }

    @Then("the merchant is registered with DTU Pay with CPR {string}")
    public void the_merchant_is_registered_with_DTU_Pay_with_cpr(String cpr) {
        Assertions.assertEquals(merchant.getCprNumber(), cpr);
    }

    @Then("the balance of the customer at the bank is {int} kr")
    public void the_balance_of_the_customer_at_the_bank_is_kr(int amount) throws BankServiceException_Exception {
        Assertions.assertEquals(BigDecimal.valueOf(amount), bank.getAccount(customerAccountId).getBalance());
    }

    @Then("the balance of the merchant at the bank is {int} kr")
    public void the_balance_of_the_merchant_at_the_bank_is_kr(int amount) throws BankServiceException_Exception {
        Assertions.assertEquals(BigDecimal.valueOf(amount), bank.getAccount(merchantAccountId).getBalance());
    }

    @Then("the exception {string} is received")
    public void the_exception_is_received(String string) {
        Assertions.assertEquals(string, exception);
    }

    @After
    public void after() {
        try {
            bank.retireAccount(customerAccountId);
        } catch (Exception e) {
            System.out.println("Account already retired");
        }

        try {
            bank.retireAccount(merchantAccountId);
        } catch (Exception e) {
            System.out.println("Account already retired");
        }

        try {
            bank.retireAccount(customerAccountId1);
        } catch (Exception e) {
            System.out.println("Account already retired");
        }

        try {
            bank.retireAccount(merchantAccountId1);
        } catch (Exception e) {
            System.out.println("Account already retired");
        }

        try {
            bank.retireAccount(customerAccountId2);
        } catch (Exception e) {
            System.out.println("Account already retired");
        }

        try {
            bank.retireAccount(merchantAccountId2);
        } catch (Exception e) {
            System.out.println("Account already retired");
        }

        try {
            bank.retireAccount(customerAccountId3);
        } catch (Exception e) {
            System.out.println("Account already retired");
        }

        try {
            bank.retireAccount(merchantAccountId3);
        } catch (Exception e) {
            System.out.println("Account already retired");
        }
    }

    User c1, c2, c3, m1, m2, m3;
    String customerAccountId1, customerAccountId2, customerAccountId3, merchantAccountId1, merchantAccountId2, merchantAccountId3;
    String customerId1, customerId2, customerId3, merchantId1, merchantId2, merchantId3;
    ArrayList<Token> tokens1, tokens2, tokens3;
    Token chosenToken1, chosenToken2, chosenToken3;
    boolean successful1, successful2, successful3;


    @Given("customer c{int} with a bank account with balance {int}")
    public void customerCWithABankAccountWithBalance(int c, int b) throws BankServiceException_Exception {
        switch (c) {
            case 1:
                try {
                    bank.retireAccount(bank.getAccountByCprNumber("31313131").getId());
                } catch (Exception e){
                    e.printStackTrace();
                }
                c1 = new User();
                c1.setCprNumber("31313131");
                c1.setFirstName("Spongebob");
                c1.setLastName("Squarepants");
                customerAccountId1 = bank.createAccountWithBalance(c1, BigDecimal.valueOf(b));
                break;
            case 2:
                try {
                    bank.retireAccount(bank.getAccountByCprNumber("32323232").getId());
                } catch (Exception e){
                    e.printStackTrace();
                }
                c2 = new User();
                c2.setCprNumber("32323232");
                c2.setFirstName("Patrick");
                c2.setLastName("Star");
                customerAccountId2 = bank.createAccountWithBalance(c2, BigDecimal.valueOf(b));
                break;
            case 3:
                try {
                    bank.retireAccount(bank.getAccountByCprNumber("33333333").getId());
                } catch (Exception e){
                    e.printStackTrace();
                }
                c3 = new User();
                c3.setCprNumber("33333333");
                c3.setFirstName("Squidward");
                c3.setLastName("Tentacles");
                customerAccountId3 = bank.createAccountWithBalance(c3, BigDecimal.valueOf(b));
                break;
            default:
                break;
        };
    }

    @Given("the customer c{int} is registered with DTU Pay")
    public void theCustomerCIsRegisteredWithDTUPay(int c) {
        switch (c) {
            case 1:
                customerId1 = customerAPI.register(c1.getFirstName() + " " + c1.getLastName(),
                        c1.getCprNumber(), customerAccountId1);
                break;
            case 2:
                customerId2 = customerAPI.register(c2.getFirstName() + " " + c2.getLastName(),
                        c2.getCprNumber(), customerAccountId2);
                break;
            case 3:
                customerId3 = customerAPI.register(c3.getFirstName() + " " + c3.getLastName(),
                        c3.getCprNumber(), customerAccountId3);
                break;
            default:
                break;
        };
    }

    @Given("merchant m{int} with a bank account with balance {int}")
    public void merchantMWithABankAccountWithBalance(int m, int b) throws BankServiceException_Exception {
        switch (m) {
            case 1:
                try {
                    bank.retireAccount(bank.getAccountByCprNumber("34343434").getId());
                } catch (Exception e){
                    e.printStackTrace();
                }
                m1 = new User();
                m1.setCprNumber("34343434");
                m1.setFirstName("Eugene");
                m1.setLastName("Krabs");
                merchantAccountId1 = bank.createAccountWithBalance(m1, BigDecimal.valueOf(b));
                break;
            case 2:
                try {
                    bank.retireAccount(bank.getAccountByCprNumber("35353535").getId());
                } catch (Exception e){
                    e.printStackTrace();
                }
                m2 = new User();
                m2.setCprNumber("35353535");
                m2.setFirstName("Sandy");
                m2.setLastName("Cheeks");
                merchantAccountId2 = bank.createAccountWithBalance(m2, BigDecimal.valueOf(b));
                break;
            case 3:
                try {
                    bank.retireAccount(bank.getAccountByCprNumber("36363636").getId());
                } catch (Exception e){
                    e.printStackTrace();
                }
                m3 = new User();
                m3.setCprNumber("36363636");
                m3.setFirstName("Penelope");
                m3.setLastName("Puff");
                merchantAccountId3 = bank.createAccountWithBalance(m3, BigDecimal.valueOf(b));
                break;
            default:
                break;
        };
    }

    @Given("that the merchant m{int} is registered with DTU Pay")
    public void thatTheMerchantMIsRegisteredWithDTUPay(int m) {
        switch (m) {
            case 1:
                merchantId1 = merchantAPI.register(m1.getFirstName() + " " + m1.getLastName(),
                        m1.getCprNumber(), merchantAccountId1);
                break;
            case 2:
                merchantId2 = merchantAPI.register(m2.getFirstName() + " " + m2.getLastName(),
                        m2.getCprNumber(), merchantAccountId2);
                break;
            case 3:
                merchantId3 = merchantAPI.register(m3.getFirstName() + " " + m3.getLastName(),
                        m3.getCprNumber(), merchantAccountId3);
                break;
            default:
                break;
        };
    }

    @Given("that the customer c{int} gets {int} tokens from the DTU Pay system")
    public void thatTheCustomerCGetsTokensFromTheDTUPaySystem(int c, int t) throws CustomersServiceException {
        switch (c) {
            case 1:
                tokens1 = customerAPI.getTokens(t, customerId1);
                break;
            case 2:
                tokens2 = customerAPI.getTokens(t, customerId2);
                break;
            case 3:
                tokens3 = customerAPI.getTokens(t, customerId3);
                break;
            default:
                break;
        }
    }

    @Given("that customer c{int} chooses one of the tokens")
    public void thatCustomerCChoosesOneOfTheTokens(int c) {
        switch (c) {
            case 1:
                if (!tokens1.isEmpty()) {
                    chosenToken1 = (Token) tokens1.get(0);
                } else {
                    Assertions.fail();
                }
                break;
            case 2:
                if (!tokens2.isEmpty()) {
                    chosenToken2 = (Token) tokens2.get(0);
                } else {
                    Assertions.fail();
                }
                break;
            case 3:
                if (!tokens3.isEmpty()) {
                    chosenToken3 = (Token) tokens3.get(0);
                } else {
                    Assertions.fail();
                }
                break;
            default:
                break;
        }
    }
    
    @When("the merchant m{int} initiates a payment for {int} kr by the customer")
    public void theMerchantMInitiatesAPaymentForKrByTheCustomer(int m, int a) {

        switch (m){
            case 1:
                thread1 = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            successful1 = merchantAPI.pay(chosenToken1, merchantId1, a);
                        } catch (Exception e) {
                            e.printStackTrace();
                            successful1 = false;
                        }
                    }
                });
                break;
            case 2:
                thread2 = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            successful2 = merchantAPI.pay(chosenToken2, merchantId2, a);
                        } catch (Exception e) {
                            e.printStackTrace();
                            successful2 = false;
                        }
                    }
                });
                break;
            case 3:
                    thread3 = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                successful3 = merchantAPI.pay(chosenToken3, merchantId3, a);
                            } catch (Exception e) {
                                e.printStackTrace();
                                successful3 = false;
                            }
                        }
                    });
                break;
            default:
                break;
        }
    }

    @When("the payments start concurrently")
    public void the_payments_start_concurrently() {
        thread1.start();
        thread2.start();
        thread3.start();

        while (thread1.isAlive() || thread2.isAlive() || thread3.isAlive()) {}
    }

    @Then("the payment is successful for customer c{int} and merchant m{int}")
    public void thePaymentIsSuccessfulForCustomerCAndMerchantM(int c, int m) throws InterruptedException {
        String switchKeys = c + "," + m;
        switch (switchKeys) {
            case "1,1":
                Assertions.assertTrue(successful1);
                break;
            case "2,2":
                Assertions.assertTrue(successful2);
                break;
            case "3,3":
                Assertions.assertTrue(successful3);
                break;
            default:
                break;
        }
    }

    @Given("the balance of the customer c{int} at the bank is {int} kr")
    public void theBalanceOfTheCustomerCAtTheBankIsKr(int c, int a) {
        switch (c) {
            case 1:
                try {
                    Assertions.assertEquals(BigDecimal.valueOf(a), bank.getAccount(customerAccountId1).getBalance());
                } catch (BankServiceException_Exception e) {
                    e.printStackTrace();
                }
                break;
            case 2:
                try {
                    Assertions.assertEquals(BigDecimal.valueOf(a), bank.getAccount(customerAccountId2).getBalance());
                } catch (BankServiceException_Exception e) {
                    e.printStackTrace();
                }
                break;
            case 3:
                try {
                    Assertions.assertEquals(BigDecimal.valueOf(a), bank.getAccount(customerAccountId3).getBalance());
                } catch (BankServiceException_Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }

    @Given("the balance of the merchant m{int} at the bank is {int} kr")
    public void theBalanceOfTheMerchantMAtTheBankIsKr(int m, int a) {
        switch (m) {
            case 1:
                try {
                    Assertions.assertEquals(BigDecimal.valueOf(a), bank.getAccount(merchantAccountId1).getBalance());
                } catch (BankServiceException_Exception e) {
                    e.printStackTrace();
                }
                break;
            case 2:
                try {
                    Assertions.assertEquals(BigDecimal.valueOf(a), bank.getAccount(merchantAccountId2).getBalance());
                } catch (BankServiceException_Exception e) {
                    e.printStackTrace();
                }
                break;
            case 3:
                try {
                    Assertions.assertEquals(BigDecimal.valueOf(a), bank.getAccount(merchantAccountId3).getBalance());
                } catch (BankServiceException_Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }


}
