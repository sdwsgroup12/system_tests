package customer;

// @author Ion s204710
public class CustomersServiceException extends Exception {

        public CustomersServiceException() {}

        public CustomersServiceException(String string) {
            super(string);
        }

        private static final long serialVersionUID = 1L;
}
