package customer;

import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import model.Report;
import model.Token;
import model.TokenRequest;

import java.util.ArrayList;

// @author Matteo s222952
public class CustomerAPI {
    Client c = ClientBuilder.newClient();
    WebTarget r = c.target("http://localhost:8080/");

    public CustomerAPI() {
    }

    public String register(String name, String cpr, String customerAccountId) {
        model.Client customer = new model.Client();
        customer.setName(name);
        customer.setCpr(cpr);
        customer.setAccountId(customerAccountId);
        return r.path("customers")
                .request()
                .post(Entity.entity(customer, MediaType.APPLICATION_JSON), String.class);
    }

    public ArrayList<Token> getTokens(int amountTokens, String customerId) throws CustomersServiceException {
        TokenRequest tokenRequest = new TokenRequest();
        tokenRequest.setCustomerId(customerId);
        tokenRequest.setAmount(amountTokens);
        try {
            return r.path("customers/tokens").queryParam("amountTokens", amountTokens).queryParam("customerId", customerId)
                    .request()
                    .post(Entity.entity(tokenRequest, MediaType.APPLICATION_JSON),new GenericType<ArrayList<Token>>() {});

        } catch (BadRequestException e) {
            var response = e.getResponse();
            var error = response.readEntity(String.class);
            throw new CustomersServiceException(error);
        }
    }

    public Report getReport(String customerId) throws Exception {
       try{
           return r.path("customers/reports/" + customerId)
                .request()
                .get(new GenericType<Report>() {
                });
       } catch (BadRequestException e) {
            var response = e.getResponse();
            var error = response.readEntity(String.class);
            throw new Exception(error);
       }
    }

    public boolean deregister(String customerId) {
        Response response = r.path("customers/" + customerId)
                .request()
                .delete();

        return response.getStatus() == 204;
    }
}