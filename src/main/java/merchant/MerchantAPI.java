package merchant;

import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import model.PaymentRequest;
import model.Report;
import model.Token;
import model.Transaction;

// @author Cesare s232435
public class MerchantAPI {

    Client c = ClientBuilder.newClient();
    WebTarget r = c.target("http://localhost:8080/");

    public String register(String name, String cpr, String merchantAccountId) {
        model.Client merchant = new model.Client();
        merchant.setName(name);
        merchant.setCpr(cpr);
        merchant.setAccountId(merchantAccountId);
        return r.path("merchants")
                .request()
                .post(Entity.entity(merchant, MediaType.APPLICATION_JSON), String.class);
    }

    public boolean pay(Token chosenToken, String merchantId, int amount) throws Exception {
        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setMerchantID(merchantId);
        paymentRequest.setAmount(amount);
        paymentRequest.setToken(chosenToken);

        try {
            Transaction payment = r.path("merchants/payment/")
                    .request()
                    .post(Entity.entity(paymentRequest, MediaType.APPLICATION_JSON), Transaction.class);
            return payment != null;
        } catch (BadRequestException e) {

            var response = e.getResponse();
            var error = response.readEntity(String.class);
            throw new Exception(error);
        }
    }

    public Report getReport(String merchantId) throws Exception {
        try {
            return r.path("merchants/reports/" + merchantId)
                    .request()
                    .get(new GenericType<Report>() {
                    });
        } catch (BadRequestException e) {
            var response = e.getResponse();
            var error = response.readEntity(String.class);
            throw new Exception(error);
        }
       
    }


    public boolean deregister(String merchantId) {
        Response response = r.path("merchants/" + merchantId)
                    .request()
                    .delete();
        return response.getStatus() == 204;
    }
}