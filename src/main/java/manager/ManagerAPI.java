package manager;

import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import model.Report;

public class ManagerAPI {
    Client c = ClientBuilder.newClient();
    WebTarget r = c.target("http://localhost:8080/");

    public Report getReport() {
        return r.path("reports/")
                .request()
                .get(new GenericType<Report>() {
                });
    }


}
