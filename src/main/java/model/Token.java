package model;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public class Token implements Serializable {
    private static final long serialVersionUID = 9096319357261571727L;
    private String tokenID;

    public String getTokenID() {
        return tokenID;
    }
    public void setTokenID(String tokenID) {
        this.tokenID = tokenID;
    }

    public void setRandomID() {
        this.tokenID = UUID.randomUUID().toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Token token = (Token) o;
        return Objects.equals(tokenID, token.tokenID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tokenID);
    }
}
