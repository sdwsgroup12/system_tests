package model;

import com.google.common.base.Objects;

public class ReportRequest {
    private static final long serialVersionUID = 4725114166170157390L;
    private String clientId;

    public ReportRequest() {
    }

    public ReportRequest(String clientId) {
        this.clientId = clientId;
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientID(String clientId) {
        this.clientId = clientId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReportRequest that = (ReportRequest) o;
        return Objects.equal(clientId, that.clientId);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(clientId);
    }

    @Override
    public String toString() {
        return "ReportRequest{" +
                "clientId='" + clientId + '\'' +
                '}';
    }
}
