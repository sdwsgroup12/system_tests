package model;

import java.io.Serializable;
import java.util.Objects;

public class PaymentRequest implements Serializable {
    private static final long serialVersionUID = 1063463902311449770L;
    private String merchantID;
    private Token token;
    private int amount;


    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PaymentRequest that = (PaymentRequest) o;
        return amount == that.amount && Objects.equals(merchantID, that.merchantID) && Objects.equals(token, that.token);
    }

    @Override
    public int hashCode() {
        return Objects.hash(merchantID, token, amount);
    }
}
