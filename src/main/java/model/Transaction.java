package model;

import java.io.Serializable;
import java.util.Objects;

public class Transaction implements Serializable {
    private static final long serialVersionUID = 1322208117830771623L;
    private String merchantAccountId;
    private String customerAccountId;

    private Payment payment;


    public String getMerchantAccountId() {
        return merchantAccountId;
    }

    public void setMerchantAccountId(String merchantAccountId) {
        this.merchantAccountId = merchantAccountId;
    }

    public String getCustomerAccountId() {
        return customerAccountId;
    }

    public void setCustomerAccountId(String customerAccountId) {
        this.customerAccountId = customerAccountId;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return Objects.equals(merchantAccountId, that.merchantAccountId) && Objects.equals(customerAccountId, that.customerAccountId) && Objects.equals(payment, that.payment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(merchantAccountId, customerAccountId, payment);
    }
}
