package model;

import com.google.common.base.Objects;

import java.io.Serializable;
import java.util.UUID;

public class Payment implements Serializable {
    private static final long serialVersionUID = 733100150515343905L;
    private String paymentID;
    private String customerID;
    private PaymentRequest paymentRequest;


    public String getPaymentID() {
        return paymentID;
    }

    public void setPaymentID(String paymentID) {
        this.paymentID = paymentID;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public PaymentRequest getPaymentRequest() {
        return paymentRequest;
    }

    public void setPaymentRequest(PaymentRequest paymentRequest) {
        this.paymentRequest = paymentRequest;
    }

    public void generatePaymentId() {
        this.paymentID = UUID.randomUUID().toString();

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Payment payment = (Payment) o;
        return Objects.equal(paymentID, payment.paymentID) && Objects.equal(customerID, payment.customerID) && Objects.equal(paymentRequest, payment.paymentRequest);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(paymentID, customerID, paymentRequest);
    }

    @Override
    public String toString() {
        return "Payment{" +
                "paymentID='" + paymentID + '\'' +
                ", customerID='" + customerID + '\'' +
                ", paymentRequest=" + paymentRequest +
                '}';
    }
}
